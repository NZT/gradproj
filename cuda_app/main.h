#include <stdio.h>
#include <sys/mman.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <cuda_runtime.h>
#include "keycode.h"
#define WRITE_PAGE 0x40000000
#define SEND_BUFFER 0x50000000

#define OFFSET(a) ((a) & 0xFFF)

struct key_struct {
  char compose;
  char _null;
  char key[6];
};

const char* b2k(char b) {
  const char* a;
  int i = (int)b;

  if (keytable_len > i) {
    a = keycode[i];
    if (a != NULL) return a;
  }
  
  return "{UNKNOWN}";
}

void printMod(char k) {
  if( k & LCTRLCODE )
    printf("<LCTRL>");
  if ( k & LSHIFTCODE)
    printf("<LSHIFT>");
  if ( k & LALT)
    printf("<LALT>");
  if ( k & LWIN)
    printf("<LWIN>");
  if( k & RCTRLCODE )
    printf("<RCTRL>");
  if ( k & RSHIFTCODE)
    printf("<RSHIFT>");
  if ( k & RALT)
    printf("<RALT>");
  if ( k & RWIN)
    printf("<RWIN>");
}

void hexDump (char *desc, void *addr, int len) {
    int i;
    unsigned char buff[17];
    unsigned char *pc = (unsigned char*)addr;

    // Output description if given.
    if (desc != NULL)
        printf ("%s:\n", desc);

    if (len == 0) {
        printf("  ZERO LENGTH\n");
        return;
    }
    if (len < 0) {
        printf("  NEGATIVE LENGTH: %i\n",len);
        return;
    }

    // Process every byte in the data.
    for (i = 0; i < len; i++) {
        // Multiple of 16 means new line (with line offset).

        if ((i % 16) == 0) {
            // Just don't print ASCII for the zeroth line.
            if (i != 0)
                printf ("  %s\n", buff);

            // Output the offset.
            printf ("  %04x ", i);
        }

        // Now the hex code for the specific character.
        printf (" %02x", pc[i]);

        // And store a printable ASCII character for later.
        if ((pc[i] < 0x20) || (pc[i] > 0x7e))
            buff[i % 16] = '.';
        else
            buff[i % 16] = pc[i];
        buff[(i % 16) + 1] = '\0';
    }

    // Pad out last line if not exactly 16 characters.
    while ((i % 16) != 0) {
        printf ("   ");
        i++;
    }

    // And print the final ASCII bit.
    printf ("  %s\n", buff);
}
