#include <cuda_runtime.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#define SIZE 100

__global__ void VectorAdd(int *input, int *output)
{
	int i = 0;
	printf("Start!\n");
	
	for(i = 0; i < 10000; i++)
	{
		*output = i;		
	}
}

int main()
{
	int input[1], output[1];

	*input=0;
	*output =0 ;
	
	int *d_a;
	int *d_o;

	cudaMalloc(&d_a, sizeof(int));
	cudaMalloc(&d_o, sizeof(int));

	//cudaMemcpy(d_a, &a, sizeof(int), cudaMemcpyHostToDevice);
	while(1)
	{
	VectorAdd <<<1,1>>>(d_a, d_o);
	}//VectorAdd <<<1,1, 0, stream>>>(d_a, d_o);

	printf("StartHost\n");	
	for(int i =0 ; i < 1000; i++)
	{
		cudaMemcpy(output, d_o, SIZE*sizeof(int), cudaMemcpyDeviceToHost);
		printf("[=]%d\n", *output);
	}

	printf("Wait!\n");

	cudaFree(d_a);
	cudaFree(d_o);
	return 0;
}

