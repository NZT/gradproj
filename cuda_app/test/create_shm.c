#include <stdio.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define KEY_NUM 9527
#define MEM_SIZE 1024

int main()
{
	int shm_id;
	void *shm_addr;
	int count;

	shm_id = shmget((key_t)KEY_NUM, MEM_SIZE, IPC_CREAT|0666);
	if(shm_id == -1)
	{
		printf("[-]Create shared memory failed!\n");
		return -1;
	}

	shm_addr = shmat(shm_id, (void *)0, 0);
	
	if(shm_addr == -1)
	{
		printf("[-]Fetch shared memory failed!\n");
		return -1;
	}

	count = 0;

	while(1)
	{
		sprintf((char *)shm_addr, "%d", count++);
		printf("%d\n", count);
		sleep(1);
	}

	return 0;
}
