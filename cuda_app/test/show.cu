#include <stdio.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/ipc.h>

#define KEY_NUM 9527
#define MEM_SIZE 1024

int main(void)
{
	int shm_id;
	void *shm_addr;

	shm_id = shmget((key_t)KEY_NUM, MEM_SIZE, IPC_CREAT|0666);
	
	if(shm_id == -1)
	{
		printf("[-]Create shared memory failed!\n");
		return -1;
	}

	shm_addr = shmat(shm_id, (void *)0, 0);
	if(shm_addr == -1)
	{
		printf("[-]Fetch shared memory failed!\n");
		return -1;
	}

	while(1)
	{
		printf("%s\n", (char *)shm_addr);
		sleep(1);
	}

	return 1;
}
