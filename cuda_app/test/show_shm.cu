#include <stdio.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <cuda_runtime.h>

#define KEY_NUM 9527
#define MEM_SIZE 1024

__global__ void Logdata(int *i, int *o)
{
	o[0] =  i[0];
}

int main(void)
{
	int shm_id;
	void *shm_addr;
	cudaError_t err;
	int *ip;

	shm_id = shmget((key_t)KEY_NUM, MEM_SIZE, IPC_CREAT|0666);
	
	if(shm_id == -1)
	{
		printf("[-]Create shared memory failed!\n");
		return -1;
	}

	shm_addr = shmat(shm_id, (void *)0, 0);
	if(shm_addr == (void *)-1)
	{
		printf("[-]Fetch shared memory failed!\n");
		return -1;
	}

	int output[1];
	int *log;
	cudaMalloc(&log, sizeof(int));

	cudaHostRegister(shm_addr, sizeof(shm_addr), cudaHostRegisterMapped);
	
	err = cudaHostGetDevicePointer((void **)&ip, (void *)shm_addr, 0);
	printf("%d\n",err);

	while(1)
	{
		Logdata <<< 1, 1 >>> (ip, log);

		cudaThreadSynchronize();
		err = cudaGetLastError();
		if(err != cudaSuccess)
		{
			printf("[-] Failed! (err = %d)\n", err);
			exit(EXIT_FAILURE);
		}
		cudaMemcpy(output, log, sizeof(int), cudaMemcpyDeviceToHost);
		printf("%s\n", output);
		usleep(8000);
	}

	return 1;
}
