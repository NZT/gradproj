#include <cuda_runtime.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#define SIZE 1

__global__ void VectorAdd(int *input, int *output)
{
	int i = 0;
	printf("Start!\n");
	
	for(i = 0; i < SIZE; i++)
	{
		printf("-----0x%llx\n", input);
		output[0] = *input;
	}
}

int main()
{
	int *input, output[100];
	cudaError_t err;
	
	int *d_a;
	int *d_o;

	cudaHostAlloc((void**) &input, 32, cudaHostAllocMapped);

	printf("ijput 0x%llx\n", input);
	printf("0x%llx\n", d_a);
	err = cudaHostGetDevicePointer(&d_a, (void *)input, 0);

	d_a[4] = 1;
	printf("Err : %d\n", err);
	printf("0x%llx\n", d_a);
	printf("0x%llx\n", input);
	printf("0x%llx\n", d_a[4]);
	printf("0x%llx\n", input[4]);
	cudaMalloc(&d_o, sizeof(int));

	//cudaMemcpyAsync(d_a, &input, sizeof(int), cudaMemcpyHostToDevice, stream);
	
	//VectorAdd <<<1,1>>>(d_a, d_o);
	VectorAdd <<<1,SIZE>>>(d_a, d_o);

	printf("StartHost\n");	

	for(int i =0 ; i < 10; i++)
	{
		//*input = i;
		cudaMemcpy(output, d_o, SIZE*sizeof(int), cudaMemcpyDeviceToHost);
		printf("[=]%d\n", *output);
	}

	printf("Wait!\n");

	cudaFree(d_a);
	cudaFree(d_o);
	return 0;
}

