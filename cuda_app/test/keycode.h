#define LCTRLCODE 0x01
#define RCTRLCODE 0x10
#define LSHIFTCODE 0x02
#define RSHIFTCODE 0x20
#define LALT 0x04
#define RALT 0x40
#define LWIN 0x08
#define RWIN 0x80

const char *keycode[] = {
0,    // 00
0,    // 01
0,    // 02
0,    // 03
"a",    // 04* a
"b",    // 05* b
"c",    // 06* c
"d",    // 07* d
"e",    // 08* e
"f",    // 09* f
"g",    // 0A* g
"h",    // 0B* h
"i",    // 0C* i
"j",    // 0D* j
"k",    // 0E* k
"l",    // 0F* l (ell)
"m",    // 10* m
"n",    // 11* n
"o",    // 12* o
"p",    // 13* p
"q",    // 14* q
"r",    // 15* r
"s",    // 16* s
"t",    // 17* t
"u",    // 18* u
"v",    // 19* v
"w",    // 1A* w
"x",    // 1B* x
"y",    // 1C* y
"z",    // 1D* z
"1",    // 1E* 1
"2",    // 1F* 2
"3",    // 20* 3
"4",    // 21* 4
"5",    // 22* 5
"6",    // 23* 6
"7",    // 24* 7
"8",    // 25* 8
"9",    // 26* 9
"0",    // 27* 0
"{ENTER}",    // 28* BT (ENTER key)
"{ESC}",    // 29*  ESC
"{BACKSPACE}",    // 2A* Backspace
"{TAB}",    // 2B* Tab (AA)
" ",    // 2C* space
"-",    // 2D* -  (dash)
"=",    // 2E* =
"[",    // 2F* [
"]",    // 30* ]
"\\",    // 31* backspace, but using fwd slash for now to avoid "escape"
"m",    // 32  m
";",    // 33* ;
"'",    // 34* " (single quote)
"~",    // 35* ~
",",    // 36* , (comma)
".",    // 37* . (period)
"/",    // 38* /
"{CAPSLOCK}",    // 39* ID  (caps lock key)
"{F1}",      // 3A* F1
"{F2}",      // 3B* F2
"{F3}",      // 3C* F3
"{F4}",      // 3D* F4
"{F5}",      // 3E* F5
"{F6}",      // 3F* F6
"{F7}",      // 40* F7
"{F8}",      // 41* F8
"{F9}",      // 42* F9
"{F10}",      // 43* F10
"{F11}",      // 44* F11
"{F12}",      // 45* F12
"{PScSRq}",      // 46
"{SCROLLLOCK}",      // 47* scroll lock
"{PAUSE}",      // 48  keypad 8
"{INSERT}",      // 49* Insert Key (EQUAL)
"{HOME}",      // 4A  keypad -
"{PGUP}",      // 4B  keypad 4
"{DELETE}",      // 4C* Delete
"{END}",      // 4D* End
"{PGDN}",      // 4E* PgDn
"{UP}",      // 4F* right arrow (UP   1)
"{LEFT}",      // 50* left  arrow (DOWN 1)
"{DOWN}",      // 51* down  arrow (DOWN 5)
"{UP}",      // 52* up    arrow (UP   5)
"{NUMLOCK}",      // 53* NumLock (toggle auto ctrl)
"{NUM/}",      // 54
"{NUM*}",      // 55
"{NUM-}",      // 56
"{NUM+}",      // 57  F11
"{NUMENT}",      // 58* keypad ENTER (QRX)
"{NUM1}",      // 59* keypad 1
"{NUM2}",      // 5A* keypad 2
"{NUM3}",      // 5B* keypad 3
"{NUM4}",      // 5C* keypad 4
"{NUM5}",      // 5D* keypad 5
"{NUM6}",      // 5E* keypad 6
"{NUM7}",      // 5F* keypad 7
"{NUM8}",      // 60* keypad 8
"{NUM9}",      // 61* keypad 9
"{NUM0}",      // 62* keypad 0
"{NUM.}",      // 63  prtscr
0,      // 64  Right Alt
"{OPTION}",      // 65* (APPS) clear display
0,      // 66  home
0,      // 67  Up Arrow
0,      // 68  page up
0,      // 69  Left Arrow
0,      // 6A  Right Arrow
0,      // 6B  end (KBD)
0,      // 6C  Down Arrow
0,      // 6D  pgdn (WT)
0,      // 6E
0,      // 6F  delete (PDL)
0,      // 70
0,      // 71
0,      // 72
0,      // 73
0,      // 74
0,      // 75
0,      // 76
0,      // 77  break
0,      // 78
0,      // 79
0,      // 7A
0,      // 7B
0,      // 7C
0,      // 7D
0,      // 7E
0,      // 7F
};

const int keytable_len = sizeof(keycode)/sizeof(char*);
