#include "main.h"
#include "mmlist.h"

#define TARGET_PROC "keylog"
struct mmlist mml;
struct task_struct *task;

int g_time_interval = 100;
struct timer_list g_timer;

void _TimerHandler(unsigned long data) {
  struct vm_area_struct* cur_vm = task->mm->mmap;
//  struct mmlist* cur = &mml;

  while(cur_vm) {
    if ((long long unsigned int)cur_vm->vm_start >= 0x7F0000000000) {
      cur_vm = cur_vm->vm_next;
      continue;
    }

    if (insert_mmlist(&mml, (long long unsigned int)cur_vm->vm_start, (long long unsigned int)get_pte(task->mm, cur_vm->vm_start)->pte) == -1) {
      printk("Find anomaly page\n");
      break;
    }
    cur_vm = cur_vm->vm_next;
  }

  mod_timer( &g_timer, jiffies + msecs_to_jiffies(g_time_interval));
  /*
  while(cur) {
     printk(KERN_INFO "--------------------------------------------------------------------------------\n");
        printk(KERN_INFO "[+] Addr : %llX\n", cur->addr);
        printk(KERN_INFO "[+] PTE : %llX\n", cur->pte);
        printk(KERN_INFO "--------------------------------------------------------------------------------\n");

   cur = cur->next;
  }
  */
}

static int __init mod_init(void) {
  mml.next = 0;
  mml.addr = 0;
  mml.pte = 0;

  task = get_task(TARGET_PROC);

  if (task == NULL) {
    printk("NOT FOUND TARGET PROCESS\n");
    return 0;
  }

  setup_timer(&g_timer, _TimerHandler, 0);
  mod_timer( &g_timer, jiffies + msecs_to_jiffies(g_time_interval));


  return 0;
}

static void __exit mod_cleanup(void) {
  del_timer(&g_timer);
}


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Lee, Cheon");
MODULE_DESCRIPTION("Graduate Project");

module_init(mod_init);
module_exit(mod_cleanup);
