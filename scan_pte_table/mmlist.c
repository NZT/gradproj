#include "mmlist.h"

int insert_mmlist(struct mmlist *mml, unsigned long long addr, unsigned long long pte) {
  struct mmlist* cur = mml;
  struct mmlist* newM;

  while(cur->next != 0) {
    if (cur->addr == addr || (cur->next && cur->next->addr > addr))
      break;

    cur = cur->next;
  }

  if (cur->addr == addr) {
    if (cur->pte != pte)
      return -1;
  } else {
    newM = kmalloc(sizeof(*newM), GFP_KERNEL);
    newM->addr = addr;
    newM->pte = pte;
    newM->next = cur->next;
    cur->next = newM;
  }

  return 0;
}
