#ifndef MAIN_H
#define MAIN_H

#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros
#include <linux/version.h>
#include <linux/delay.h>
#include <linux/kallsyms.h>
#include <linux/slab.h>
#include <linux/mm_types.h>
#include <linux/sched.h>
#include <linux/list.h>
#include "mmem.h"

static int __init mod_init(void);
static void __exit mod_cleanup(void);

#endif
