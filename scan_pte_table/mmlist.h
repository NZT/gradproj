#ifndef _MMLIST_H_
#define _MMLIST_H_
#include <linux/slab.h>
#include <linux/list.h>

struct mmlist {
	unsigned long long addr;
	unsigned long long pte;
	struct mmlist* next;
};

int insert_mmlist(struct mmlist*, unsigned long long, unsigned long long);

#endif
