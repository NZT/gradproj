#include <linux/sched.h>
#include <asm/pgtable.h>
pte_t* get_pte(struct mm_struct* mm, uint64_t addr);
struct task_struct* get_task(char* target_proc_name);
void write_buf_addr_to_mem(pte_t* pte, uint64_t addr);
void write_pte(pte_t* pte, uint64_t addr);
