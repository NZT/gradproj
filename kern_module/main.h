#ifndef MAIN_H
#define MAIN_H

#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros
#include <linux/version.h>
#include <linux/kallsyms.h>
#include <linux/highmem.h>
#include <linux/sched.h>
#include "kb.h"
#include "mmem.h"

extern struct resource iomem_resource;

static int __init mod_init(void);
static void __exit mod_cleanup(void);

uint64_t KMEM_START = 0xffff880000000000;
uint64_t KMEM_END = 0xffff880280000000;
#endif
