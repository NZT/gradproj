#include "mmem.h"

struct task_struct* get_task(char* target_proc_name) {
  struct task_struct *task;

  for_each_process(task) {
    if (strcmp(task->comm, target_proc_name) == 0)
      return task;
  }

  return NULL;
}

pte_t* get_pte(struct mm_struct* mm, uint64_t addr) {
  pgd_t* pgd; pud_t* pud; pmd_t* pmd; pte_t* pte;

  pgd = pgd_offset(mm, addr);
  if (pgd_none(*pgd) || pgd_bad(*pgd)) {
    printk("bad pgd\n");
    return NULL;
  }

  pud = pud_offset(pgd, addr);
  if (pud_none(*pud) || pud_bad(*pud)) {
    printk("bad pud\n");
    return NULL;
  }

  pmd = pmd_offset(pud, addr);
  if (pmd_none(*pmd) || pmd_bad(*pmd)) {
    printk("bad pmd\n");
    return NULL;
  }

  pte = pte_offset_map(pmd, addr);
  if (pte_none(*pte) || (pte->pte & PTE_FLAGS_MASK) == 0) {
    printk("bad pte\n");
    return NULL;
  }

  return pte;
}

void write_buf_addr_to_mem(pte_t* pte, uint64_t addr) {
  uint64_t dst;
  dst = PAGE_OFFSET | (pte_val(*pte) & PAGE_MASK);
  *(uint64_t*)dst = addr;
}

void write_pte(pte_t* pte, uint64_t addr) {
  uint64_t dst;
  dst = ((addr ^ PAGE_OFFSET) & PAGE_MASK) | pte_val(*pte) & ~PAGE_MASK;
  pte->pte = dst;
}
