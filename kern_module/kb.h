#ifndef KB_H
#define KB_H

#include <linux/usb.h>
#include <linux/version.h>
#include <linux/ctype.h>
#include <asm/pgtable.h>
#include <linux/kallsyms.h>

bool find_product(char *buf);
uint64_t find_kb_buffer(uint64_t, uint64_t);
bool is_target(struct urb*);
int is_valid_addr(uint64_t);
void init(void);

#endif
